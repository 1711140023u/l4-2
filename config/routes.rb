Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'top/main'
  post 'top/login'
  root 'top#main'
  get 'top/logout'
  
  get '/top/new', to: 'top#new'
  post 'top/signin'
  resources :users, only:[:index,:new,:create,:destroy]
end
